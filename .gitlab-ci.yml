image: registry.gitlab.com/adriandc/gitlabci-local/python:3.11-alpine

stages:
  - development
  - prepare
  - build
  - test
  - quality
  - deploy

variables:
  DOCKER_HOST: tcp://docker:2375
  EXECUTOR_HOST: preview
  EXECUTOR_TOOL: gitlabci-local
  REGISTRY_HOST: registry.gitlab.com
  REGISTRY_OWNER: adriandc
  REGISTRY_PROJECT: gitlabci-local

.local:
  engine: docker,auto
  real_paths: true
  sockets: true
  variables:
    DOCKER_HOST: ''
  version: 9.0

# =============================================================================
# Stage: development

changelog:
  stage: development
  image: local:silent
  script:
    - mkdir -p ./.tmp/
    - |
      if ! ls ./.tmp/git-chglog.tar.gz >/dev/null 2>&1; then
        wget -O ./.tmp/git-chglog.tar.gz -q https://github.com/git-chglog/git-chglog/releases/download/v0.15.4/git-chglog_0.15.4_linux_amd64.tar.gz
        tar -xzf ./.tmp/git-chglog.tar.gz -C ./.tmp/
        rm -f ./.tmp/git-chglog.tar.gz
        chmod +x ./.tmp/git-chglog
      fi
    - 'echo -n " > Current commit : "'
    - git describe --always
    - echo ''
    - 'read -p " > Release tag : " -r tag'
    - echo ''
    - git tag -f -m '' "${tag}"
    - ./.tmp/git-chglog -o ./CHANGELOG.md
    - sed -i "s#raw/[^/]*/docs#raw/${tag}/docs#g" ./README.md
    - git add -v ./CHANGELOG.md ./README.md
    - 'git commit -m "docs: changelog: regenerate release tag changes history" -s'
    - git tag -f -m '' "${tag}"
    - ./.tmp/git-chglog -o ./CHANGELOG.md
    - sh ./.chglog/changelog.sh --clean
    - git add -v ./CHANGELOG.md
    - git commit --amend --no-edit
    - git tag -f -m '' "${tag}"
  only:
    - local

dependencies:
  stage: development
  image: local:quiet
  script:
    - sudo pip3 install -r ./requirements/runtime.txt --upgrade
    - sudo pip3 install -r ./requirements/build.txt --upgrade
    - sudo pip3 install -r ./requirements/coverage.txt --upgrade
    - sudo pip3 install -r ./requirements/deploy.txt --upgrade
    - sudo pip3 install -r ./requirements/quality.txt --upgrade
    - sudo pip3 install -r ./requirements/tests.txt --upgrade
    - sudo pip3 install -r ./requirements/docs.txt --upgrade
  only:
    - local

development:
  stage: development
  image: local:quiet
  script:
    - sudo rm -rf ./build ./dist ./*.egg-info ./.eggs
    - python3 ./setup.py bdist_wheel
    - pipx install --force ./dist/*.whl
  after_script:
    - sudo rm -rf ./build ./*.egg-info ./.eggs
  only:
    - local

images:
  stage: development
  image: local:quiet
  script:
    - |
      for image in \
          alpine:3 \
          alpine:3.11 \
          alpine:3.12 \
          alpine/git:latest \
          debian:testing-slim \
          docker:dind \
          docker:latest \
          python:3.6 \
          python:3.6-alpine \
          python:3.11 \
          python:3.11-alpine \
          ruby:3.1 \
          sonarsource/sonar-scanner-cli:latest \
          tobix/pywine:3.7 \
          toopher/centos-i386:centos6 \
          ubuntu:20.04 \
      ; do
        docker pull "${image}"
        docker tag "${image}" "${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/${image}"
        docker push "${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/${image}"
        docker rmi "${image}"
      done
    - |
      docker pull "${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/python:3.11-alpine"
      docker build -t "${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/gitlabci-local:preview" - <<-EOF
      FROM ${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/python:3.11-alpine
      RUN apk add docker
      EOF
      docker push "${REGISTRY_HOST}/${REGISTRY_OWNER}/${REGISTRY_PROJECT}/gitlabci-local:preview"
  only:
    - local

preview:
  stage: development
  image: registry.gitlab.com/adriandc/gitlabci-local/gitlabci-local:preview
  before_script:
    - pip3 install -q ./dist/*.whl 2>/dev/null
    - pip3 install -q -r ./requirements/docs.txt --upgrade 2>/dev/null
    - pip3 install -q -r ./requirements/tests.txt --upgrade 2>/dev/null
  script:
    - pexpect-executor -- termtosvg ./docs/preview.svg -c 'python3 ./docs/preview.py' -g 80x24 -t ./docs/template.svg
  only:
    - local

# =============================================================================
# Stage: prepare

codestyle:
  stage: prepare
  variables:
    PIP_DISABLE_PIP_VERSION_CHECK: 1
  before_script:
    - pip3 install -q -r ./requirements/quality.txt --upgrade
    - apk add -q git
  script:
    - yapf -i ./docs/*.py ./gitlabci_local/*.py ./gitlabci_local/*/*.py ./setup.py
    - unify -i --quote "'" ./docs/*.py ./gitlabci_local/*.py ./gitlabci_local/*/*.py ./setup.py
    - echo '' && git diff --name-status
    - git diff --quiet
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'docs/**/*'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
        - 'setup.py'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success
  allow_failure: true

lint:
  stage: prepare
  variables:
    PIP_DISABLE_PIP_VERSION_CHECK: 1
  before_script:
    - pip3 install -q -r ./requirements/runtime.txt --upgrade
    - pip3 install -q -r ./requirements/quality.txt --upgrade
    - pip3 install -q -r ./requirements/tests.txt --upgrade
  script:
    - pylint --errors-only ./docs/ ./gitlabci_local/ ./setup.py
    - pylint --disable=missing-docstring --load-plugins=pylint.extensions.no_self_use ./docs/ ./gitlabci_local/ ./setup.py
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'docs/**/*'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
        - 'setup.py'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success
  allow_failure: true

typings:
  stage: prepare
  variables:
    MYPY_FORCE_COLOR: 1
    PIP_DISABLE_PIP_VERSION_CHECK: 1
    TERM: ansi
  before_script:
    - apk add -q git
    - pip3 install -q -r ./requirements/runtime.txt --upgrade
    - pip3 install -q -r ./requirements/quality.txt --upgrade
    - pip3 install -q -r ./requirements/tests.txt --upgrade
  script:
    - mypy --follow-imports silent --pretty $(git diff --name-only $(git diff --exit-code >/dev/null && echo 'HEAD^' || echo 'HEAD') ./docs/ ./gitlabci_local/ ./setup.py) 2>/dev/null || true
    - sleep 1
    - mypy --follow-imports silent --pretty ./docs/ ./gitlabci_local/ ./setup.py || true
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'docs/**/*'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
        - 'setup.py'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success
  allow_failure: true

# =============================================================================
# Stage: build

build:
  stage: build
  needs: []
  variables:
    PIP_DISABLE_PIP_VERSION_CHECK: 1
  before_script:
    - pip3 install -r ./requirements/runtime.txt --upgrade
    - pip3 install -r ./requirements/build.txt --upgrade
    - apk add -q git
  script:
    - rm -rf ./build ./dist ./*.egg-info ./.eggs
    - python3 ./setup.py bdist_wheel
  after_script:
    - rm -rf ./build ./*.egg-info ./.eggs
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success
  artifacts:
    paths:
      - dist/

install:
  stage: development
  image: local:quiet
  script:
    - pipx install --force ./dist/*.whl
  only:
    - local

# =============================================================================
# Stage: test (templates)

.unit-tests-dependencies:
  before_script: &unit-tests-dependencies-before |-
    apt-get update -qq
    apt-get install -qq -y sudo time 2>&1 | grep 'Setting up'

.unit-tests-docker:
  services:
    - docker:dind
  needs:
    - build
  variables:
    DOCKER_DRIVER: overlay2
  before_script: &unit-tests-docker-before |-

.unit-tests-podman:
  needs:
    - build
  variables:
    CI_LOCAL_NETWORK: host
  before_script: &unit-tests-podman-before |-
    export DOCKER_HOST='tcp://127.0.0.1:9999'
    apt-get update -qq
    apt-get install -qq -y podman 2>&1 | grep 'Setting up podman'
    sed -i 's#.*cgroup_manager = ".*"#cgroup_manager = "cgroupfs"#g' /usr/share/containers/containers.conf
    sed -i 's#.*events_logger = ".*"#events_logger = "file"#g' /usr/share/containers/containers.conf
    sed -i 's#.*log_driver = ".*"#log_driver = "k8s-file"#g' /usr/share/containers/containers.conf
    sed -i '/# unqualified-search-registries =/a unqualified-search-registries = ["docker.io"]' /etc/containers/registries.conf
    cat >>/etc/containers/storage.conf <<EOF
    [storage]
    driver = "vfs"
    runroot = "/var/run/containers/storage"
    graphroot = "/var/run/containers/storage"
    EOF

.unit-tests-rules:
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success

.unit-tests-template:
  stage: test
  timeout: 20m
  variables:
    CI_LOCAL_UPDATES_DISABLE: 'true'
    FORCE_COLOR: 1
    TEMP: .tmp
    TERM: ansi
  before_script: &unit-tests-template-before |-
    mkdir -p ./.tmp/
    pip3 install ./dist/*.whl
    pip3 install --force-reinstall --no-deps --upgrade ./dist/*.whl
    pip3 show gitlabci-local
    pip3 install -q -r ./requirements/tests.txt --upgrade
  script:
    # Prepare tests
    - export SUITE=$(echo "${SUITE}" | sed 's/,/\\|/g')
    - if [ ! -z "${SUITE}" ]; then ls -1d ./tests/* | grep -q "\/\(${SUITE}\)" || (set +x; echo ''; echo -e '\033[1;31m[ERROR] The requested SUITE filter does not match any suite\033[0m'; echo ''; exit 1); fi
    - |
      if [ ! -z "${CI_LOCAL_HOST}" ]; then
        (
          set +x
          echo ''
          echo ' [INFO] Access to "sudo" required during sudo related tests...'
          echo ''
          sudo echo -n ''
          echo ''
        )
      fi
    # Run tests
    - for path in $(ls -1d ./tests/* | grep "\/\(${SUITE}\)"); do (cd "${path}/" && time sh ./test.sh 2>&1); done
    # Finish tests
    - |
      (
        set +x
        echo ''
        echo ' [INFO] Unit tests finished successfully'
        echo ''
      )

.coverage-template:
  extends:
    - .unit-tests-template
    - .unit-tests-rules
  variables:
    COVERAGE_COMMON: ${CI_PROJECT_DIR}/coverage-reports/coverage-common.xml
    COVERAGE_FOLDER: ${CI_PROJECT_DIR}/coverage-reports
    COVERAGE_RCFILE: ${CI_PROJECT_DIR}/.coveragerc
    PYTHONPATH: ${CI_PROJECT_DIR}
  before_script: &coverage-template-before |-
    mkdir -p "${COVERAGE_FOLDER}/"
    chmod 777 "${COVERAGE_FOLDER}"
    pip3 install -q -r ./requirements/runtime.txt -r ./requirements/coverage.txt -r ./requirements/tests.txt --no-cache-dir --upgrade 2>/dev/null
    if [ -z "${SUITE}" ]; then
      coverage erase
    fi
    coverage run --append --module --source=gitlabci_local gitlabci_local --version >/dev/null
    chmod 666 "${COVERAGE_FILE}"
    echo 'coverage run --append --module --source=gitlabci_local gitlabci_local "${@}"' | tee /usr/local/bin/gitlabci-local /usr/local/bin/gcil >/dev/null
    chmod a+x /usr/local/bin/gitlabci-local /usr/local/bin/gcil
  after_script:
    - coverage xml -o "${COVERAGE_XML}"
    - coverage report --show-missing --skip-covered
    - sed -i 's#\(<source>\).*\(</source>\)#\1.\2#g' "${COVERAGE_XML}"
    - |
      set +x
      if [ ! -z "${CI_LOCAL}" ]; then
        common=$(mktemp -d)
        cp "${COVERAGE_FOLDER}/.coverage-"* "${common}/"
        COVERAGE_FILE="${common}/.coverage" coverage combine "${common}/.coverage-"*
        COVERAGE_FILE="${common}/.coverage" coverage report --ignore-errors --show-missing --skip-covered
        COVERAGE_FILE="${common}/.coverage" coverage xml -o "${COVERAGE_COMMON}"
        sed -i 's#\(<source>\).*\(</source>\)#\1.\2#g' "${COVERAGE_COMMON}"
        rm -rf "${common}"
      fi
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
        - 'tests/**/*'
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web" || $CI_COMMIT_REF_NAME != "develop"
      when: on_success
  artifacts:
    paths:
      - coverage-reports/

.coverage-template-windows:
  before_script: &coverage-template-windows-before |-
    echo 'wine coverage "${@}"' | tee /usr/local/bin/coverage >/dev/null
    chmod a+x /usr/local/bin/coverage
    echo 'wine pip3 "${@}"' | tee /usr/local/bin/pip3 >/dev/null
    chmod a+x /usr/local/bin/pip3
    pip3 install -q --no-cache-dir --upgrade wheel 2>/dev/null
  script:
    - cd ./tests/windows/ && sh ./test.sh 2>&1

# =============================================================================
# Stage: test (coverage)

coverage:docker:
  extends:
    - .coverage-template
    - .unit-tests-docker
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.11
  needs: []
  variables:
    COVERAGE_FILE: ${CI_PROJECT_DIR}/coverage-reports/.coverage-docker
    COVERAGE_XML: ${CI_PROJECT_DIR}/coverage-reports/coverage-docker.xml
  before_script:
    - *unit-tests-docker-before
    - *unit-tests-dependencies-before
    - *coverage-template-before

coverage:podman:
  extends:
    - .coverage-template
    - .unit-tests-podman
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.11
  needs: []
  variables:
    COVERAGE_FILE: ${CI_PROJECT_DIR}/coverage-reports/.coverage-podman
    COVERAGE_XML: ${CI_PROJECT_DIR}/coverage-reports/coverage-podman.xml
  before_script:
    - *unit-tests-podman-before
    - *unit-tests-dependencies-before
    - *coverage-template-before

coverage:windows:
  extends:
    - .coverage-template
    - .coverage-template-windows
  image: registry.gitlab.com/adriandc/gitlabci-local/tobix/pywine:3.7
  needs: []
  variables:
    COVERAGE_FILE: ${CI_PROJECT_DIR}/coverage-reports/.coverage-windows
    COVERAGE_XML: ${CI_PROJECT_DIR}/coverage-reports/coverage-windows.xml
  before_script:
    - *coverage-template-windows-before
    - *coverage-template-before

# =============================================================================
# Stage: test (Docker)

py3.6:docker:
  extends:
    - .unit-tests-template
    - .unit-tests-docker
    - .unit-tests-rules
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.6
  before_script:
    - *unit-tests-docker-before
    - *unit-tests-dependencies-before
    - *unit-tests-template-before

py3.11:docker:
  extends:
    - .unit-tests-template
    - .unit-tests-docker
    - .unit-tests-rules
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.11
  before_script:
    - *unit-tests-docker-before
    - *unit-tests-dependencies-before
    - *unit-tests-template-before

# =============================================================================
# Stage: test (Podman)

py3.6:podman:
  extends:
    - .unit-tests-template
    - .unit-tests-podman
    - .unit-tests-rules
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.6
  before_script:
    - *unit-tests-podman-before
    - *unit-tests-dependencies-before
    - *unit-tests-template-before

py3.11:podman:
  extends:
    - .unit-tests-template
    - .unit-tests-podman
    - .unit-tests-rules
  image: registry.gitlab.com/adriandc/gitlabci-local/python:3.11
  before_script:
    - *unit-tests-podman-before
    - *unit-tests-dependencies-before
    - *unit-tests-template-before

# =============================================================================
# Stage: test (local)

python:dind:
  extends:
    - .unit-tests-template
  image: local:quiet
  before_script:
    - *unit-tests-template-before
    - docker rm -f gitlabci-local-dind >/dev/null 2>&1
    - docker run -d -v "${PWD}/.dind:/certs" -v "${PWD}:${PWD}" --name gitlabci-local-dind --privileged docker:dind >/dev/null
    - sleep 10
    - export DIND_IP=$(docker exec gitlabci-local-dind hostname -i)
    - export DOCKER_CERT_PATH="${PWD}/.dind/client/"
    - export DOCKER_HOST="tcp://${DIND_IP}:2376"
    - export DOCKER_TLS_VERIFY=0
  after_script:
    - docker rm -f gitlabci-local-dind >/dev/null
  only:
    - local

python:local:
  extends:
    - .unit-tests-template
  image: local:quiet
  before_script:
    - mkdir -p ./.tmp/
    - sudo pip3 install --force-reinstall --no-deps --upgrade ./dist/*.whl
    - sudo pip3 install -q -r ./requirements/tests.txt --upgrade
  only:
    - local

# =============================================================================
# Stage: quality

sonarcloud:
  stage: quality
  image:
    name: registry.gitlab.com/adriandc/gitlabci-local/sonarsource/sonar-scanner-cli:latest
    entrypoint: ['']
  needs:
    - coverage:docker
    - coverage:podman
    - coverage:windows
  variables:
    GIT_DEPTH: 0
    SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar
  cache:
    key: '${CI_JOB_NAME}'
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -Dsonar.projectVersion=$(git describe --always) -Dsonar.branch.name=${CI_COMMIT_REF_NAME}
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop"
      changes:
        - 'CHANGELOG.md'
        - 'gitlabci_local/**/*'
        - 'requirements/**/*'
        - 'tests/**/*'
      when: always
    - if: $CI_COMMIT_REF_NAME == "master"
      when: always
    - if: $CI_COMMIT_REF_NAME == "merge_requests"
      when: always  
    - when: never
  allow_failure: true

# ===============================================================================
# Stage: deploy

deploy:release:
  stage: deploy
  dependencies:
    - build
  variables:
    PIP_DISABLE_PIP_VERSION_CHECK: 1
    TWINE_PASSWORD: ${TWINE_PASSWORD}
  before_script:
    - if [ ! "${CI_COMMIT_REF_PROTECTED}" = 'true' ]; then echo '[ERROR] Please protect this tag...'; exit 1; fi
    - apk add -q gcc git libc-dev libffi-dev sed
    - pip3 install -q -r ./requirements/deploy.txt --upgrade
  script:
    - tag=${CI_COMMIT_REF_NAME:-$(git describe --always --abbrev=0)}
    - description=$(sh ./.chglog/changelog.sh "${tag}" | sed -z 's/"/\\"/g')
    - description="${description}"$'\n\n''### Download'$'\n'
    - gitlab-release --description "${description}" --link-prefix '* ' ./dist/*
    - twine upload -u '__token__' -p "${TWINE_PASSWORD}" ./dist/*
  rules:
    - if: $CI_COMMIT_TAG

deploy:trial:
  stage: deploy
  dependencies:
    - build
  variables:
    PIP_DISABLE_PIP_VERSION_CHECK: 1
    TWINE_PASSWORD_TEST: ${TWINE_PASSWORD_TEST}
  before_script:
    - pip3 install -q -r ./requirements/deploy.txt --upgrade
  script:
    - twine upload -u '__token__' -p "${TWINE_PASSWORD_TEST}" --repository testpypi ./dist/*
  only:
    - local
  tags:
    - local
  when: manual
